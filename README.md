# City Traffic Analyzer 

## Description 
City Traffic Analyzer is a demonstrative application that showcases a possible pipeline for processing data about car traffic in various cities. The first step is the aquisition of data about the movement of cars. Cars are indentified by their unique licence plate. In our demo, this is done by a simple mock genertion service. The the information about passed cars is sent to various services for further processing. These are:
 - **Foreign Car** - check for information about cars from an internation database
 - **General Storage** - store all passed cars and make analytical queries
 - **Stolen Car** - check for stolen cars and notify relavant authorities

## Possible Usage
City Traffic Analyzer could be a tool for city planners and public officials that want to learn more about the traffic in the city of their concern. The creation of date would not be done by a generator rather than acquired from scanners around the city. There are both analytical and data handling services available which could be utilized. In a real usage scenario, the Foreign and Stolen Car services would be connected to their real life counterparts.

## Why microservices?
We've chosen the microservice architecture for our project because there is high potential for scalability. The traffic in all cities is very variable by nature. During rush hour there is a much higher amount of license plates passed on street, where our potential sensors and scanner would be deployed to gather data. All this data would be sent to our implementation and the deployed system has to be able to accomodate for surges in input. Furthermore, various parts of the city have different traffic amounts in general, so we can scale according to this.

## Benefits of using microservices
 - high separation of functionality - services could be implemented independently
 - minimum of boilerplate code thanks to Quarkus and Panache
 - interesting stats thank to Grafana and Prometheus

## Drawbacks of using microservices
 - complex to set up
 - harder to understand compared to monolith
 - data was harder to track, but fortunately, we did not encounter many bugs
