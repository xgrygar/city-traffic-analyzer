package cz.muni.fi.pv217.model;

import io.smallrye.mutiny.Uni;

import javax.persistence.Entity;

@Entity
public class ForeignLicensePlate extends LicensePlate {
    public String country;

    public static Uni<ForeignLicensePlate> findByLicensePlate(String licensePlate) {
        return find("licensePlate", licensePlate).firstResult();
    }

    public static Uni<Long> countAllForeignPlates() {
        Uni<Long> countAll = ForeignLicensePlate.count();
        return countAll;
    }
}
