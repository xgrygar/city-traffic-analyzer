package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.ForeignLicensePlate;
import io.quarkus.hibernate.reactive.rest.data.panache.PanacheEntityResource;
import io.quarkus.rest.data.panache.ResourceProperties;

import javax.transaction.Transactional;

// Thanks to PanacheEntityResource the CRUD endpoint are created automatically
@ResourceProperties(path = "foreign")
@Transactional(Transactional.TxType.REQUIRED)
public interface ForeignCarPlatesResource extends PanacheEntityResource<ForeignLicensePlate, Long> {

}