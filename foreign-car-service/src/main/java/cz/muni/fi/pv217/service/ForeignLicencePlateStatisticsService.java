package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlate;
import org.jboss.resteasy.reactive.RestResponse;

import javax.enterprise.context.ApplicationScoped;

import java.time.Duration;
import java.time.Instant;
import java.time.ZoneId;

@ApplicationScoped
public class ForeignLicencePlateStatisticsService {

    public Long countLicencePlatePassesByDay(Instant instant) {
        // var time = instant.atZone(ZoneId.systemDefault());
        // TODO: somehow use date for this instead of instant
        return LicensePlate.count("passDate", instant).await().indefinitely();
    }

    public Long countLicencePlatePassesByPlate(String plate) {
        return LicensePlate.count("licensePlate", plate).await().indefinitely();
    }

    public Long countLicencePlatePassesByLocation(String location) {
        return LicensePlate.count("location", location).await().indefinitely();
    }

    public Long countAllForeignLicencePlates() {
        return LicensePlate.count().await().atMost(Duration.ofSeconds(5));
    }
}