package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlate;
import cz.muni.fi.pv217.model.ForeignLicensePlate;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

import io.micrometer.core.instrument.MeterRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/check")
@ApplicationScoped
public class CheckForeignCarResource {

    private final MeterRegistry registry;

    CheckForeignCarResource(MeterRegistry registry) {
        this.registry = registry;
    }

    private static final Logger LOG = Logger.getLogger(CheckForeignCarResource.class);

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse<Boolean> checkIsLicencePlateForeign(LicensePlate licensePlatePass) {
        var isForeign = !(licensePlatePass.licensePlate
                .matches("^(?=[0-9]*[ASULKHEPCJBMTZ])(?=.*[0-9])[A-FH-NPR-VX-Z0-9]{5,7}$"));
        if (isForeign) {
            LOG.info("Foreign license plate was found: " + licensePlatePass.licensePlate);

            registry.counter("foreign.check").increment();

            ForeignLicensePlate foreignLicensePlate = new ForeignLicensePlate();
            foreignLicensePlate.licensePlate = licensePlatePass.licensePlate;
            foreignLicensePlate.location = licensePlatePass.location;
            foreignLicensePlate.passDate = licensePlatePass.passDate;
            foreignLicensePlate.country = "others";
            foreignLicensePlate.persist();
        }
        return RestResponse.ResponseBuilder.ok(isForeign).build();
    }
}