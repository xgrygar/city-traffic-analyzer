package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlatePass;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@QuarkusTest
class LicensePlateStorageResourceTest {

    @Test
    void testAddEndpoint() {
//        Mock resource
        PanacheMock.mock(LicensePlatePass.class);

        Map<String, String> licencePlatePass = new HashMap<>();
        licencePlatePass.put("licensePlate", "6E60912");
        licencePlatePass.put("location", "Brno");
        licencePlatePass.put("stolenDate", "2022-11-06T19:54:34.499Z");

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(licencePlatePass)
                .when()
                .post("/storage")
                .then()
                .statusCode(201);
    }

    @Test
    void testGetEndpoint() {
//        Mock resource (db)
//        Be aware that with mocking you do not have any data saved. You need to mock response for get endpoints
        PanacheMock.mock(LicensePlatePass.class);

        LicensePlatePass licensePlatePass = new LicensePlatePass();
        licensePlatePass.licensePlate = "6e60229";
        licensePlatePass.passDate = new Date().toInstant();
        licensePlatePass.id = 1L;

//        Mock find by id for the get endpoint
        Mockito.when(LicensePlatePass.findById(1L)).thenReturn(Uni.createFrom().item(licensePlatePass));

        given()
                .pathParam("id", 1L)
                .when()
                .get("/storage/{id}")
                .then()
                .statusCode(200);
    }
}
