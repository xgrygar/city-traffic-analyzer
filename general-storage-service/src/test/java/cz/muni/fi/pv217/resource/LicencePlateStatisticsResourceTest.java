package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlatePass;
import cz.muni.fi.pv217.service.LicencePlateStatisticsService;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import io.restassured.RestAssured;
import io.restassured.parsing.Parser;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.MediaType;
import java.time.Instant;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.is;

@QuarkusTest
class LicencePlateStatisticsResourceTest {

    @Test
    void countLicencePlatePassesByDay() {
        RestAssured.registerParser("text/plain", Parser.TEXT);
        PanacheMock.mock(LicensePlatePass.class);
        Mockito.when(LicensePlatePass.count("passDate", Instant.parse("2022-11-06T19:54:34.499Z")))
                .thenReturn(Uni.createFrom().item(3L));

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body("\"2022-11-06T19:54:34.499Z\"")
                .when()
                .get("/stats/day")
                .then().assertThat()
                .body(is("3"))
                .statusCode(200);
    }


    @Test
    void countLicencePlatePassesByPlate() {
        RestAssured.registerParser("text/plain", Parser.TEXT);
        PanacheMock.mock(LicensePlatePass.class);
        Mockito.when(LicensePlatePass.count("licensePlate", "A"))
                .thenReturn(Uni.createFrom().item(1L));

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body("A")
                .when()
                .get("/stats/plate")
                .then().assertThat()
                .body(is("1"))
                .statusCode(200);
    }

    @Test
    void countLicencePlatePassesByLocation() {
        RestAssured.registerParser("text/plain", Parser.TEXT);
        PanacheMock.mock(LicensePlatePass.class);
        Mockito.when(LicensePlatePass.count("location", "Brno"))
                .thenReturn(Uni.createFrom().item(2L));

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body("Brno")
                .when()
                .get("/stats/location")
                .then().assertThat()
                .body(is("2"))
                .statusCode(200);
    }
}