package cz.muni.fi.pv217.model;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;

import javax.persistence.Entity;
import java.time.Instant;

@Entity
public class LicensePlatePass extends PanacheEntity {
    public String licensePlate;
    public Instant passDate;
    public String location;
}
