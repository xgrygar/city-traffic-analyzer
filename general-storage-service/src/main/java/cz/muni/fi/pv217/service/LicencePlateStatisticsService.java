package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlatePass;

import javax.enterprise.context.ApplicationScoped;
import java.time.Instant;

@ApplicationScoped
public class LicencePlateStatisticsService {

    public Long countLicencePlatePassesByDay(Instant instant) {
        // var time = instant.atZone(ZoneId.systemDefault());
        // TODO: somehow use date for this instead of instant
        return LicensePlatePass.count("passDate", instant).await().indefinitely();
    }

    public Long countLicencePlatePassesByPlate(String plate) {
        return LicensePlatePass.count("licensePlate", plate).await().indefinitely();
    }

    public Long countLicencePlatePassesByLocation(String location) {
        return LicensePlatePass.count("location", location).await().indefinitely();
    }
}
