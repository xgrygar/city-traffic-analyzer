package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.service.LicencePlateStatisticsService;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

import io.micrometer.core.instrument.MeterRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/stats")
@ApplicationScoped
public class LicencePlateStatisticsResource {

    @Inject
    LicencePlateStatisticsService licencePlateStatisticsService;

    private static final Logger LOG = Logger.getLogger(LicencePlateStatisticsResource.class);

    private final MeterRegistry registry;

    LicencePlateStatisticsResource(MeterRegistry registry) {
        this.registry = registry;
    }

    @GET
    @Path("/day")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse<Long> countLicencePlatePassesByDay(Instant instant) {
        registry.counter("general.stats.request.day").increment();

        var count = licencePlateStatisticsService.countLicencePlatePassesByDay(instant);
        LOG.info(count + " cars have passed at " + instant);
        return RestResponse.ResponseBuilder.ok(count).build();
    }

    @GET
    @Path("/plate")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse<Long> countLicencePlatePassesByPlate(String plate) {
        registry.counter("general.stats.request.plate").increment();

        var count = licencePlateStatisticsService.countLicencePlatePassesByPlate(plate.strip());
        LOG.info("Car with plate " + plate + " has passed " + count + " times");
        return RestResponse.ResponseBuilder.ok(count).build();
    }

    @GET
    @Path("/location")
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse<Long> countLicencePlatePassesByLocation(String location) {
        registry.counter("general.stats.request.location").increment();

        var count = licencePlateStatisticsService.countLicencePlatePassesByLocation(location.strip());
        LOG.info("At location " + location + ", " + count + " cars have passed");
        return RestResponse.ResponseBuilder.ok(count).build();
    }
}
