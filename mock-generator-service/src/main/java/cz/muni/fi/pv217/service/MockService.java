package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlatePass;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MockService {

    @RestClient
    DistributorService distributorService;

    public void distributeLicencePlate(LicensePlatePass licensePlatePass) {
        distributorService.distributeLicencePlate(licensePlatePass);
    }

}
