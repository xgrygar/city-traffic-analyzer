package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlatePass;
import cz.muni.fi.pv217.service.MockService;

import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

import com.mifmif.common.regex.Generex;
import com.oracle.svm.core.annotate.Inject;

import io.micrometer.core.instrument.MeterRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.time.Instant;

@Path("/mock")
@ApplicationScoped
public class MockGeneratorResource {

    @Inject
    MockService mockService;

    private static final Logger LOG = Logger.getLogger(MockGeneratorResource.class);

    private final MeterRegistry registry;

    MockGeneratorResource(MeterRegistry registry) {
        this.registry = registry;
    }

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public RestResponse startMock() {
        registry.counter("mocking.mock").increment();
        LOG.info("Generation started");
        Generex generex = new Generex("[A-Z0-9]{5,7}");

        Generex cityGenerex = new Generex("(Brno)|(Praha)|(Ostrava)");
        String city = cityGenerex.random();
        // Using Generex iterator
        String randomPlate = generex.random();
        mockService.distributeLicencePlate(new LicensePlatePass(randomPlate,
                Instant.now(), city));
        LOG.info("\n" + city);
        LOG.info("\n" + randomPlate);
        LOG.info("Generation ended");
        return RestResponse.ResponseBuilder.ok().build();
    }
}