package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlatePass;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/distribute")
@RegisterRestClient(configKey = "distributor-api")
public interface DistributorService {

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    void distributeLicencePlate(LicensePlatePass licensePlatePass);
}
