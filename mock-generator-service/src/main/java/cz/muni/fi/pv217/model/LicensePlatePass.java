package cz.muni.fi.pv217.model;

import java.time.Instant;

public class LicensePlatePass {
    public String licensePlate;
    public Instant passDate;
    public String location;

    public LicensePlatePass(String licensePlate, Instant passDate, String location) {
        this.licensePlate = licensePlate;
        this.passDate = passDate;
        this.location = location;
    }
}
