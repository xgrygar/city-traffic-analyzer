package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.StolenLicensePlate;
import io.quarkus.hibernate.reactive.rest.data.panache.PanacheEntityResource;
import io.quarkus.rest.data.panache.ResourceProperties;

import javax.transaction.Transactional;

// Thanks to PanacheEntityResource the CRUD endpoint are created automatically
@ResourceProperties(path = "stolen")
@Transactional(Transactional.TxType.REQUIRED)
public interface StolenCarStorageResource extends PanacheEntityResource<StolenLicensePlate, Long> {

}