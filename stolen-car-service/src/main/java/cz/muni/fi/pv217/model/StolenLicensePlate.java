package cz.muni.fi.pv217.model;

import io.quarkus.hibernate.reactive.panache.PanacheEntity;
import io.smallrye.mutiny.Uni;

import javax.persistence.Entity;
import java.time.Instant;

@Entity
public class StolenLicensePlate extends PanacheEntity {
    public String licensePlate;
    public Instant stolenDate;

    public  static Uni<StolenLicensePlate> findByLicensePlate(String licensePlate) {
        return find("licensePlate", licensePlate).firstResult();
    }
}
