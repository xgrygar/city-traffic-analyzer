package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlatePass;
import cz.muni.fi.pv217.service.StolenCarStorageService;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

import io.micrometer.core.instrument.MeterRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/check")
@ApplicationScoped
public class CheckStolenCarResource {

    @Inject
    StolenCarStorageService stolenCarStorageService;

    private static final Logger LOG = Logger.getLogger(CheckStolenCarResource.class);

    private final MeterRegistry registry;

    CheckStolenCarResource(MeterRegistry registry) {
        this.registry = registry;
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse<Boolean> checkIsLicencePlateStolen(LicensePlatePass licensePlatePass) {
        registry.counter("stolen.check").increment();

        LOG.info("License plate passed: " + licensePlatePass.licensePlate);
        var isStolen = stolenCarStorageService.checkStolenLicensePlate(licensePlatePass.licensePlate);
        return RestResponse.ResponseBuilder.ok(isStolen).build();
    }
}