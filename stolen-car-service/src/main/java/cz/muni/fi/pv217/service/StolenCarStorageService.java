package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.StolenLicensePlate;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class StolenCarStorageService {

    public boolean checkStolenLicensePlate(String licensePlate) {
        var stolenLicensePlate = StolenLicensePlate.findByLicensePlate(licensePlate).await().indefinitely();
        return stolenLicensePlate != null;
    }

}
