package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.StolenLicensePlate;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@QuarkusTest
class StolenCarStorageResourceTest {

    @Test
    void testAddEndpoint() {
//        Mock resource
        PanacheMock.mock(StolenLicensePlate.class);

        Map<String, String> stolenLicensePlate = new HashMap<>();
        stolenLicensePlate.put("licensePlate", "6E60912");
        stolenLicensePlate.put("stolenDate", "2022-11-06T19:54:34.499Z");

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(stolenLicensePlate)
                .when()
                .post("/stolen")
                .then()
                .statusCode(201);
    }

    @Test
    void testGetEndpoint() {
//        Mock resource (db)
//        Be aware that with mocking you do not have any data saved. You need to mock response for get endpoints
        PanacheMock.mock(StolenLicensePlate.class);

        StolenLicensePlate stolenLicensePlate = new StolenLicensePlate();
        stolenLicensePlate.licensePlate = "6e60229";
        stolenLicensePlate.stolenDate = new Date().toInstant();
        stolenLicensePlate.id = 1L;

//        Mock find by id for the get endpoint
        Mockito.when(StolenLicensePlate.findById(1L)).thenReturn(Uni.createFrom().item(stolenLicensePlate));

        given()
                .pathParam("id", 1L)
                .when()
                .get("/stolen/{id}")
                .then()
                .statusCode(200);
    }

}