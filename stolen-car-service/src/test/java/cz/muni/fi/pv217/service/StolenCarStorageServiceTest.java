package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.StolenLicensePlate;
import io.quarkus.panache.mock.PanacheMock;
import io.quarkus.test.junit.QuarkusTest;
import io.smallrye.mutiny.Uni;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.inject.Inject;
import javax.ws.rs.core.MediaType;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

@QuarkusTest
class StolenCarStorageServiceTest {

    @Inject
    StolenCarStorageService service;

    @Test
    void testCheckStolenLicensePlateIsFalse() {
        Assertions.assertFalse(service.checkStolenLicensePlate("6e60923"));
    }

    @Test
    void testCheckStolenLicensePlateIsTrue() {
//        Mock resource (db)
//        Be aware that with mocking you do not have any data saved. You need to mock response for get endpoints
        PanacheMock.mock(StolenLicensePlate.class);

        StolenLicensePlate stolenLicensePlate = new StolenLicensePlate();
        stolenLicensePlate.licensePlate = "6e60229";
        stolenLicensePlate.stolenDate = new Date().toInstant();
        stolenLicensePlate.id = 1L;

//        Mock find by id for the get endpoint
        Mockito.when(StolenLicensePlate.findByLicensePlate("6e60229")).thenReturn(Uni.createFrom().item(stolenLicensePlate));


        Assertions.assertTrue(service.checkStolenLicensePlate("6e60229"));
    }

}