package cz.muni.fi.pv217.resource;

import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class CheckStolenCarResourceTest {

    @Test
    void testCheckEndpoint() {
        Map<String,String> licensePlatePass = new HashMap<>();
        licensePlatePass.put("licensePlate", "6E60912");
        licensePlatePass.put("passDate", "2022-11-06T19:54:34.499Z");
        licensePlatePass.put("location", "Chrudim");

        given()
                .contentType(MediaType.APPLICATION_JSON)
                .body(licensePlatePass)
                .when().post("/check")
                .then()
                .body(is("false"))
                .statusCode(200);
    }

}