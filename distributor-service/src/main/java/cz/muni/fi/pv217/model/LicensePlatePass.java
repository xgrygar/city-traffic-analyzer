package cz.muni.fi.pv217.model;

import java.time.Instant;

public class LicensePlatePass {
    public String licensePlate;
    public Instant passDate;
    public String location;
}
