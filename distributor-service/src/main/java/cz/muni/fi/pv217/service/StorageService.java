package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlatePass;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/storage")
@RegisterRestClient(configKey="storage-api")
public interface StorageService {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    void storeLicencePlate(LicensePlatePass licensePlatePass);
}
