package cz.muni.fi.pv217.resource;

import cz.muni.fi.pv217.model.LicensePlatePass;
import cz.muni.fi.pv217.service.DistributorService;
import org.jboss.logging.Logger;
import org.jboss.resteasy.reactive.RestResponse;

import io.micrometer.core.instrument.MeterRegistry;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/distribute")
@ApplicationScoped
public class DistributorResource {

    @Inject
    DistributorService distributorService;

    private static final Logger LOG = Logger.getLogger(DistributorResource.class);

    private final MeterRegistry registry;

    DistributorResource(MeterRegistry registry) {
        this.registry = registry;
    }

    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    public RestResponse checkIsLicencePlateStolen(LicensePlatePass licensePlatePass) {
        registry.counter("distribute.check").increment();

        LOG.info("License plate passed: " + licensePlatePass.licensePlate);
        distributorService.distributeLicencePlate(licensePlatePass);
        return RestResponse.ResponseBuilder.ok().build();
    }
}