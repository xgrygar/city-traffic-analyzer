package cz.muni.fi.pv217.service;

import cz.muni.fi.pv217.model.LicensePlatePass;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class DistributorService {

    @RestClient
    StolenCarService stolenCarService;
    @RestClient
    StorageService storageService;
    @RestClient
    ForeignCarService foreignCarService;

    public void distributeLicencePlate(LicensePlatePass licensePlatePass) {
        stolenCarService.checkLicencePlate(licensePlatePass);
        storageService.storeLicencePlate(licensePlatePass);
        foreignCarService.checkLicencePlate(licensePlatePass);
    }

}

